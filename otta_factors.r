# Factors in OTTA data dictionary
otta.histology.revised <-
  c(
    "1H" = "hgsoc",
    "1L" = "lgsoc",
    "1" = "serous.unknown.grade",
    "2" = "mucinous",
    "3" = "endometrioid",
    "4" = "clear.cell",
    "5" = "mixed.cell",
    "6" = "other.specified.epithelial.ovarian.cancer",
    "7" = "undifferentiated/poorly.differentiated.epithelial",
    "8" = "unknown.but.known.to.be.epithelial",
    "9" = "non-epithelial",
    "0" = "other",
    "99" = "serous.lmp",
    "999" = "not.OC",
    "77" = "normal.fallopian.tube",
    "888" = "borderline.tumor.with.microinvasion",
    "88" = "DK",
    "20" = "mucinous.lmp",
    "30" = "endometrioid.lmp",
    "40" = "clear.cell.lmp",
    "50" = "mixed.cell.lmp",
    "60" = "other.specified.epithelial.ovarian.cancer.lmp",
    "70" = "undifferentiated/poorly.differentiated.epithelial.lmp"
  )

otta.grade <- c(
  "1" = "well differentiated",
  "2" = "moderately differentiated",
  "3"= "poorly differentiated",
  "4" = "undifferentiated",
  "7" = "not applicable (LMP cases only)",
  "8" = "DK",
  "9" = "neo-adjuvant",
  "5" = "high grade",
  "6" = "low grade"
  )

otta.stage <- c(
  "0" = "in situ",
  "1" = "localized",
  "2" = "regional",
  "3" = "distant",
  "88" = "unstaged or DK"
  )

otta.TP53 <-
  c(
    "0" = "complete absence",
    "1" = "wild-type",
    "2" = "overexpression",
    "3" = "cytoplasmic",
    "8" = "complete absence without internal control",
    "9" = "non-interpretable"
  )
