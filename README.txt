README

.R files needed:

utils_plyr.R
otta_factors.R

.xls files needed:
overall_analysis
SEARCH_HGSOC_OTTA

Rmd files to be executed in the following order

1. search-montfort-datacleaning.Rmd
2. tp53.Rmd
3. SEARCH-Montfort_quality.Rmd
4. Manuscript.Rmd
5. Manuscript_tables.Rmd
6. SEARCH-Montfort_pca.Rmd
7. supplementary_tables.Rmd