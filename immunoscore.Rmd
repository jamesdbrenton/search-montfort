---
title: "immunoscore"
author: "Stephanie Owen"
date: "20 July 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r, echo=FALSE, message=FALSE}
rm(list=ls())
require(knitr)
#checkpoint("2016-07-01", R.version = "3.3.1")
#library(checkpoint)
#source('montage.R')
require(tidyr)
require(dplyr)
require(ggplot2)
require(gplots)
require(pander)

require(EBImage)
require(ggfortify)
require(survival)
require(survminer)

```

Load useful functions and data.

```{r loaddata, echo=FALSE}
load("./temp/search-montfort.RData")
```

```{r functions and globals, echo=FALSE}

#folder structure from within zipped files
#file.path these?

foldercd8='/Screenshots_CD8'
foldercd68='/Screenshots CD68 Analysis'
foldercd45ro = '/Screenshots'
folderALL = '/Screenshots_ALL'

resz_image = 400

resizer <- function(x) {
     resize(x, w=resz_image, h=resz_image)
 }

list_me_roi <- function(folder, x){
  list.files(paste0(temp.dir,folder, '/ROIDetection'),pattern=x)
}


list_me_cell <- function(folder, x){
  list.files(paste0(temp.dir,folder, '/CellularAnalysis'),pattern=x)
}

# display_overlay - display image and create text overlay
# input = list of images

display_overlay <- function(list){

list = lapply(list, resizer)
  
matrix = EBImage::combine(list)
display(matrix, method = 'raster', all=T)

sqrt_dim = ceiling(sqrt(dim(matrix)[4]))
IDs = matrix(1:sqrt_dim^2, ncol = sqrt_dim, byrow = T)
y = (row(IDs)-1)*dim(matrix)[2]+350
x = (col(IDs)-1)*dim(matrix)[1]+50
text(x,y,IDs, cex = 1.5, col = 'black')

}

zero_na <- function(val){
  return(ifelse(is.na(val),0,val))
}

# Splits marker density into two groups and plots - not in use yet - not generalisable enough atm

#Survival curve data frame (KM)
scc <- function(x, morph, roi){
  df = x %>% 
  dplyr::filter(vital.status %in% c(1,2), morphology.group==morph, roi.type==roi, counts.per.mm2!=Inf) %>%
  mutate(quartile = ntile(counts.per.mm2, 4), tertile=ntile(counts.per.mm2,3), median=ntile(counts.per.mm2, 2))
  df
}
```


```{r functions, echo=FALSE}
scc_comb <- function(x, morph){
  
  df = x %>% 
    dplyr::filter(vital.status %in% c(1,2), morphology.group==morph) %>% 
    mutate(tt8 = ntile(cd8.tumour, 2),
           tt45ro=ntile(cd45.tumour,2),
           ts68=ntile(cd68.stroma, 2),
           tt68=ntile(cd68.tumour, 2),
           ts45ro=ntile(cd45.stroma, 2))
  
  max.8t <- max(x$cd8.tumour, na.rm=TRUE)
  max.45ros <- max(x$cd45.stroma, na.rm=TRUE)
  max.45rot <- max(x$cd45.tumour, na.rm=TRUE)
  max.68s <- max(x$cd68.stroma, na.rm=TRUE)
  

  
 # df <- df %>% mutate(combined_discrete = rowSums(.[38:41], na.rm = TRUE)/rowSums(!is.na(.[38:41])))
  df <- df %>% 
    mutate(combined_discrete = rowMeans(.[38:41], na.rm = TRUE) ,
           log10norm_8 = log10(cd8.tumour+2)/log10(max.8t),
           log10norm_45 = 2*log10(cd45.stroma+2)/log10(max.45ros) ,  
           log10norm_68 = 7*log10(cd68.stroma+0.005)/log10(max.68s),
           log10norm_45t = log10(cd45.tumour+2)/log10(max.45rot)) %>%

    mutate(combined_continuous = rowMeans(.[43:46], na.rm = TRUE),
           ratio_8_68 = log10norm_8/log10norm_68,
           ratio_8_45ro = log10norm_8/log10norm_45,
           ratio_8_45rot = log10norm_8/log10norm_45t,
           tp_pten = ifelse(pten.if.bin=='low' & tp53.mut.class=='lof', 1, 0),
           tp_brca = ifelse(brca.status==0 & tp53.mut.class=='lof', 1, 0))

  
  return(df)
                      
}



surv_curve_comb <- function(x, morph, roi) {

df=scc_comb(x, morph, roi)
df$group = 'other'
if (roi=='tumour') {
  df$group[which(df$tertile8==1 & df$tertile45ro==1)]='CD8 and CD45RO low'
  df$group[which(df$tertile8==3 & df$tertile45ro==3)]='CD8 and CD45RO high'
}
if(roi=='stroma'){
  df$group[which(df$tertile68==1 & df$tertile45ro==1)]='CD68 and CD45RO low'
  df$group[which(df$tertile68==3 & df$tertile45ro==3)]='CD68 and CD45RO high'
}

fit = survfit(Surv(months.to.entry.x,months.to.status.x,vital.status.x)~group, data=df) #survdiff(Surv(months.to.status-months.to.entry, vital.status) ~ group, data=scc)
x_c = as.character(substitute(x))
morph_c = as.character(substitute(morph))
roi_c = as.character(substitute(roi))

file_name = gsub("\\\\", "", paste0(x_c,morph_c,roi_c,"_KM_comb.pdf"))

label_p = round(1-pchisq(survdiff(Surv(months.to.status.x, vital.status.x)~group, data=df)$chisq,2),3)

figure = autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))
ggsave(plot = figure, filename = file_name, device = pdf)
survdiff(Surv(months.to.status.x, vital.status.x)~group, data=df)

}

```


```{r immunoscore dfs, echo=FALSE}



sub_8 <-  cd8.data %>% dplyr::select(-roi.area, -roi.nuclei, -rel.area, -uid, -var.number) %>% spread(roi.type, counts.per.mm2) %>% rename(cd8.tumour = tumour, cd8.stroma=stroma) %>% dplyr::select(id, cd8.tumour, cd8.stroma)

sub_68 <- cd68.data %>% dplyr::select(-roi.area, -roi.nuclei, -rel.area, -uid, -var.number) %>% spread(roi.type, counts.per.mm2) %>% rename(cd68.tumour = tumour, cd68.stroma=stroma)%>% dplyr::select(id, cd68.tumour, cd68.stroma)

sub_45 <- cd45ro.data %>% dplyr::select(-roi.area, -roi.nuclei, -rel.area, -uid, -var.number) %>% spread(roi.type, counts.per.mm2) %>% rename(cd45.tumour = tumour, cd45.stroma=stroma) %>% dplyr::select(id, cd45.tumour, cd45.stroma)

counts_all <- full_join(sub_8, sub_68) %>% full_join(. , sub_45)

```

```{r joinsall, echo=FALSE, message=FALSE, warning=FALSE}

search.clinical <- search.clinical %>% mutate(morphology.group=factor(morphology.group, labels =c('CC', 'End', 'HGSC','LGSC', 'Mixed', 'Muc', 'Other') ))   %>% mutate(vital.status = ifelse(months.to.status>120, 1, vital.status)) %>% mutate(months.to.status = ifelse(months.to.status>120, 120, months.to.status)) %>% mutate(vital.status = ifelse(months.to.status>months.to.entry,vital.status,NA))

cd8.data <- left_join(cd8.data, search.clinical, by = c("id" = "storage.number"))
cd8.data <- left_join(cd8.data, search.tp53, by = c("id" = "srl.storage.id"))

cd45ro.data <- left_join(cd45ro.data, search.clinical, by = c("id" = "storage.number"))
cd45ro.data <- left_join(cd45ro.data, search.tp53, by = c("id" = "srl.storage.id"))

cd68.data <- left_join(cd68.data, search.clinical, by = c("id" = "storage.number"))
cd68.data <- left_join(cd68.data, search.tp53, by = c("id" = "srl.storage.id"))

cd8.data <- cd8.data %>% filter(morphology.group %in% kEpithelialHistotype)
cd45ro.data <- cd45ro.data %>% filter(morphology.group %in% kEpithelialHistotype)
cd68.data <- cd68.data %>% filter(morphology.group %in% kEpithelialHistotype)

counts_all <- left_join(counts_all, search.clinical, by = c("id" = "storage.number"))
counts_all <- left_join(counts_all, search.tp53, by = c("id" = "srl.storage.id"))

```

```{r, echo=FALSE}
surv_df <- scc_comb(counts_all, 'HGSC')

#add globals (use df, match columns)
#surv_df<-read.csv('~/SEARCHcomplete_globals_survival_data.csv')

#add cd68 

# CD8 

both <- cd8.data %>%
  filter(ihc.marker == "CD8" ) %>% 
  select(id, months.to.entry, months.to.status, vital.status, morphology.group, stage, roi.type, counts.per.mm2, tp53.mut.class, brca.status, pten.if, pten.if.bin, pten.ihc) %>%
  spread(roi.type, counts.per.mm2)

both_ratio <- both  %>% 
  filter(tumour!=0, !is.na(stroma)) %>%
  na.omit() %>%
  mutate(ratio = log10(stroma+0.05)/log10(tumour+0.05))

both_area <- cd8.data %>%
  filter(ihc.marker == "CD8") %>% 
  select(id, roi.type, roi.area) %>%
  spread(roi.type, roi.area) %>% 
  select(id, tumour_area = tumour, stroma_area=stroma)

global_8 <- full_join(both, both_area, by=c('id'))
global_8 <- mutate(global_8, global=(zero_na(tumour*tumour_area) + zero_na(stroma*stroma_area))/(zero_na(tumour_area)+zero_na(stroma_area)))


both <- cd45ro.data %>%
  filter(ihc.marker == "CD45RO") %>% 
  select(id, months.to.entry, months.to.status, vital.status, morphology.group, stage, roi.type, counts.per.mm2, tp53.mut.class, brca.status, pten.if, pten.if.bin) %>%
  spread(roi.type, counts.per.mm2)

both_ratio <- both  %>% 
  filter(tumour!=0, !is.na(stroma)) %>%
  na.omit() %>%
  mutate(ratio = log10(stroma+0.5)/log10(tumour+0.5))

both_area <- cd45ro.data %>%
  filter(ihc.marker == "CD45RO") %>% 
  select(id, roi.type, roi.area) %>%
  spread(roi.type, roi.area) %>% 
  select(id, tumour_area = tumour, stroma_area=stroma)

global_45 <- full_join(both, both_area, by=c('id'))
global_45 <- mutate(global_45, global=(zero_na(tumour*tumour_area) + zero_na(stroma*stroma_area))/(zero_na(tumour_area)+zero_na(stroma_area)))

both <- cd68.data %>%
  filter(ihc.marker == "CD68") %>% 
  select(id, months.to.entry, months.to.status, vital.status, morphology.group, stage, roi.type, counts.per.mm2, tp53.mut.class, brca.status, pten.if, pten.if.bin) %>%
  spread(roi.type, counts.per.mm2)

both_ratio <- both  %>% 
  filter(tumour!=0, !is.na(stroma)) %>%
  na.omit() %>%
  mutate(ratio = log10(stroma+0.005)/log10(tumour+0.005))

both_area <- cd68.data %>%
  filter(ihc.marker == "CD68") %>% 
  select(id, roi.type, roi.area) %>%
  spread(roi.type, roi.area) %>% 
  select(id, tumour_area = tumour, stroma_area=stroma)

global_68 <- full_join(both, both_area, by=c('id'))
global_68 <- mutate(global_68, global=(zero_na(tumour*tumour_area) + zero_na(stroma*stroma_area))/(zero_na(tumour_area)+zero_na(stroma_area)))

#add globals to surv_df

surv_df$global68 <- NA
surv_df$global68 <- global_68$global[match(surv_df$id, global_68$id)]

surv_df$global8 <- NA
surv_df$global8 <- global_8$global[match(surv_df$id, global_8$id)]

surv_df$global45 <- NA
surv_df$global45 <- global_45$global[match(surv_df$id, global_45$id)]
```

```{r, echo=FALSE}

coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd8.tumour+2), data=surv_df )

#Combine discrete measures of immune infiltration (high, med, low) and combine (to high medium low average)

surv_df$combined_discrete_tertile = ntile(surv_df$combined_discrete,2)

fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~combined_discrete_tertile, data=surv_df)

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~combined_discrete_tertile, data=surv_df)$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))


coxph(Surv(months.to.entry, months.to.status, vital.status)~combined_discrete, data=surv_df)

coxph(Surv(months.to.entry, months.to.status, vital.status)~combined_discrete+stage, data=surv_df)
#6.37e-12

test_proportionality <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(global8+0.05) + log10(global45+0.05) + stage + age.at.diagnosis, data=surv_df)

test.ph <- cox.zph(test_proportionality)
test.ph

#Combine continuous

coxph(Surv(months.to.entry, months.to.status, vital.status)~combined_continuous, data=surv_df)
#Loglikep = 0.00085, combined_contp = 0.00084

coxph(Surv(months.to.entry, months.to.status, vital.status)~combined_continuous+stage, data=surv_df)
#Loglikep = 2.62e-14, combined_contp = 3.2e-5


coxph(Surv(months.to.entry, months.to.status, vital.status)~log10norm_8+stage, data=surv_df)

coxph(Surv(months.to.entry, months.to.status, vital.status)~log10norm_45+stage, data=surv_df)

coxph(Surv(months.to.entry, months.to.status, vital.status)~log10norm_68+stage, data=surv_df)

```
```{r, echo=FALSE}
#PTEN + all immune
df<-surv_df %>% filter(!is.na(pten.ihc.bin)) 
df$combined_ptencd8 <- ifelse(df$combined_discrete_tertile>1 & df$pten.ihc.bin=='high', 1, 0) 

fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~combined_ptencd8, data=df)

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~combined_ptencd8, data=df)$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))


#p53 + all immune 
df<-surv_df %>% filter(!is.na(tp53.mut.class)&tp53.mut.class!='ND' ) 
df$combined_p53cd8 <- ifelse(df$combined_discrete_tertile==1 & df$tp53.mut.class=='lof', 1, 0) 

fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~combined_p53cd8, data=df)

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~combined_p53cd8, data=df)$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))


#combined assoc. with p53? pten?

ks = kruskal.test(combined_continuous ~ pten.ihc.bin, data = surv_df %>% filter(!is.na(pten.ihc.bin)) %>% mutate(pten.ihc.bin = as.factor(pten.ihc.bin)))
ks

ks = kruskal.test(combined_continuous ~ tp53.mut.class, data = surv_df %>% filter(!is.na(tp53.mut.class) & tp53.mut.class !='ND') %>% mutate(tp53.mut.class = as.factor(tp53.mut.class)))
ks



ks = kruskal.test(combined_continuous ~ brca.status, data = surv_df %>% filter(!is.na(brca.status)) %>% mutate(brca.status = as.factor(brca.status)))
ks



ks = kruskal.test(combined_continuous ~ tp_brca, data = surv_df)
ks
ks = kruskal.test(log10norm_8 ~ tp_brca, data = surv_df)
ks
ks = kruskal.test(log10norm_68 ~ tp_brca, data = surv_df)
ks
ks = kruskal.test(log10norm_45 ~ tp_brca, data = surv_df)
ks

ks = kruskal.test(combined_continuous ~ tp_pten, data = surv_df)
ks
ks = kruskal.test(log10norm_8 ~ tp_pten, data = surv_df)
ks
ks = kruskal.test(log10norm_68 ~ tp_pten, data = surv_df)
ks
ks = kruskal.test(log10norm_45 ~ tp_pten, data = surv_df)
ks

summary(surv_df$cd8.tumour)
summary(surv_df$log10norm_8)
summary(surv_df$cd68.stroma)
summary(surv_df$log10norm_68)
summary(surv_df$cd45.stroma)
summary(surv_df$log10norm_45)
summary(surv_df$cd45.tumour)
summary(surv_df$log10norm_45t)


fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~tp_pten, data=surv_df)

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~tp_pten, data=surv_df)$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))

fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~tp_brca, data=surv_df)

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~tp_brca, data=surv_df)$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))

fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~pten.if.bin, data=surv_df)

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~pten.if.bin, data=surv_df)$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))

fit = survfit(Surv(months.to.entry,months.to.status,vital.status)~tp53.mut.class, data=surv_df %>% filter(tp53.mut.class %in% c('gof', 'lof')))

label_p = round(1-pchisq(survdiff(Surv(months.to.status, vital.status)~tp53.mut.class, data=surv_df %>% filter(tp53.mut.class %in% c('gof', 'lof')))$chisq,2),3)

autoplot(fit, xlab='Time (Months)', ylab='', title='OS', pVal=TRUE) + annotate(x = 10, y=0.1, geom = 'text', label=paste0('p=',label_p))
```
```{r, echo=FALSE}

surv_df_chi <- surv_df%>%filter(vital.status%in%c(1,2), !is.na(ts68))
fit0 <- coxph(Surv(months.to.status, vital.status) ~ factor(ts68), iter=0, data=surv_df_chi,  na.action=na.exclude) 
  o.minus.e <- tapply(resid(fit0), surv_df_chi$ts68, sum) 
  obs       <- tapply(surv_df_chi$vital.status-1, surv_df_chi$ts68, sum) 
  table_chi <- cbind(observed=obs, expected= obs- o.minus.e, "o-e"=o.minus.e) 
  
table_chi

chisq.test(table_chi[,1], table_chi[,2])
```


```{r, echo=FALSE}
#surv_df<-read.csv('~/SEARCHcomplete_globals_survival_data.csv')

#p-values for survival curves
diff <- survdiff(Surv(months.to.status, vital.status) ~ factor(ts68), data=surv_df,  na.action=na.exclude) 
pchisq(diff$chisq, length(diff$n)-1, lower.tail = FALSE)

diff <- survdiff(Surv(months.to.status, vital.status) ~ factor(tt68), data=surv_df,  na.action=na.exclude) 
pchisq(diff$chisq, length(diff$n)-1, lower.tail = FALSE)

diff <- survdiff(Surv(months.to.status, vital.status) ~ factor(ts45ro), data=surv_df, na.action=na.exclude) 
pchisq(diff$chisq, length(diff$n)-1, lower.tail = FALSE)

diff <- survdiff(Surv(months.to.status, vital.status) ~ factor(tt45ro), data=surv_df, na.action=na.exclude)
pchisq(diff$chisq, length(diff$n)-1, lower.tail = FALSE)
```


```{r, echo=FALSE}

surv_df_orig <- surv_df

surv_df <- surv_df %>% filter(!is.na(cd8.tumour), !is.na(cd68.stroma), !is.na(cd45.tumour), !is.na(cd45.stroma), !is.na(cd68.tumour), !is.na(cd8.stroma))
#datasets -> 
coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd68.stroma+0.005)+stage, data=surv_df)
#1e-10 

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd8.stroma+0.05)+ stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd45.tumour+0.05)+ stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(global8+0.05)+ stage, data=surv_df)
fit
extractAIC(fit)
#aic = 1952.925

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd8.tumour+0.05)+stage, data=surv_df)
fit
extractAIC(fit)
#aic = 1283

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(global8+2)+stage, data=surv_df%>%filter())
fit
extractAIC(fit)
#1.2e-12
#aic = 2037

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(global45+2)+stage, data=surv_df)
fit
extractAIC(fit)
#9e-13
#aic = 2008.9

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(global45+2)+log10(global8+2)+stage, data=surv_df)
fit
extractAIC(fit)
#1.5e-12

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~global45:global8+stage, data=surv_df)
fit
extractAIC(fit)
#2e-10


fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10norm_68: log10(global8+0.05)+log10norm_68:log10(global45+0.05)+stage, data=surv_df)
fit
extractAIC(fit)
#2e-10
#1.2e-9

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(global45+2)+log10(cd68.stroma+0.005)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd68.stroma+0.005)+log10(global8+2)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd68.stroma+0.005)+log10(cd8.tumour+2)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd68.stroma+0.005)+log10(cd45.stroma+2)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd8.tumour+2)+log10(cd45.stroma+2)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd8.tumour+2)+log10(cd45.tumour+2)+log10(cd68.stroma+0.005)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd8.tumour+2)+stage, data=surv_df)
fit
extractAIC(fit)

fit <- coxph(Surv(months.to.entry, months.to.status, vital.status)~log10(cd68.stroma+0.005)+stage, data=surv_df)
fit
extractAIC(fit)



cor.test(surv_df$global45, surv_df$global8)
#pcor.test(surv_df$global45, surv_df$cd68.stroma, surv_df$global8, na.rm = TRUE)


surv_df <- surv_df_orig

```

```{r vectorised model}
library(survival)

## Create outcome variable, then survival object
pbc <- within(surv_df, {
    survival    <- Surv(months.to.entry, months.to.status, vital.status)
})


## Create vectors for outcome and predictors
outcome    <- c("survival")
predictors <- c("cd8.tumour", "cd68.stroma","cd45.stroma", "cd45.tumour", "cd8.stroma", "cd68.tumour")
dataset    <- pbc


## The lines below should not need modification.

## Create list of models
  left.hand.side  <- outcome
    right.hand.side <- paste("stage + ", predictors, sep='')
list.of.models <-    paste(left.hand.side, right.hand.side, sep = "  ~  ")


## Convert to a vector
vector.of.models <- unlist(list.of.models)

## Fit coxph to all models
list.of.fits <- lapply(vector.of.models, function(x) {

    formula    <- as.formula(x)
    fit        <- coxph(formula, data = dataset)
    result.AIC <- extractAIC(fit)

    data.frame(num.predictors = result.AIC[1],
               AIC            = result.AIC[2],
               model          = x)
})

## Collapse to a data frame
result <- do.call(rbind, list.of.fits)


## Sort and print
library(doBy)
orderBy(~ AIC, result)
dot.df <- orderBy(~ AIC, result)

ggplot(dot.df, aes(x = AIC,  y = model)) +
   geom_dotplot(binaxis = "y", stackgroups = TRUE, binwidth = .1, method = "histodot")
```



```{r warmtumours, echo=FALSE}

dim(surv_df)
summary(surv_df$cd8.stroma)
summary(surv_df$cd8.tumour)
surv_df %>% filter(!is.na(cd8.stroma), !is.na(cd8.tumour)) %>% dim()
surv_df %>% filter(cd8.stroma !=0,  cd8.tumour==0) %>% dim()
ids <- surv_df %>% filter(cd8.stroma>10*cd8.tumour) %>% select(id)

```

```{r dotplot, echo=FALSE, eval=FALSE}
dotchart(modeldf$aic, labels=row.names(modeldf),cex=.7,
  	main="AIC for survival models", 
   xlab="AIC")
```
